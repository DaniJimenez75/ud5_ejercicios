/**
 * 
 */
package ejercicio7;

/**
 * @author Dani
 *
 */
public class Ejercicio7App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		int contador = 1;
		while(contador <= 100) {
			System.out.println(contador);
			contador++; //Incrementamos contador
			
		}

	}

}
