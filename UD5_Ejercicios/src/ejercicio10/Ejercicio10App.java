/**
 * 
 */
package ejercicio10;

import javax.swing.JOptionPane;

/**
 * @author Dani
 *
 */
public class Ejercicio10App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String entrada = JOptionPane.showInputDialog("Introduce numero de ventas");
		
		//Pasamos a numero las ventas
		int ventas = Integer.parseInt(entrada);
		
		int contador = 1;
		
		double sumaTotal = 0;
		
		//Bucle while para ir sumando los valores de los productos
		while(contador<=ventas) {
		 entrada = JOptionPane.showInputDialog("Introduce valor del producto "+contador);
		double valor = Double.parseDouble(entrada);
		sumaTotal = sumaTotal + valor;
		contador++; //Incrementamos contador

		}
		
		JOptionPane.showMessageDialog(null, "La suma total de todas las ventas es:  "+sumaTotal);


	}

}
