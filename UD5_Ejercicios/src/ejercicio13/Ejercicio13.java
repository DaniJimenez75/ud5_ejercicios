/**
 * 
 */
package ejercicio13;

import javax.swing.JOptionPane;

/**
 * @author Dani
 *
 */
public class Ejercicio13 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//Introducimos datos y los pasamos a double los operadores
		String entrada = JOptionPane.showInputDialog("Introduce operador 1");
		double operador1 = Double.parseDouble(entrada);
		
		entrada = JOptionPane.showInputDialog("Introduce operador 2");
		double operador2 = Double.parseDouble(entrada);

		String aritmetico = JOptionPane.showInputDialog("Introduce signo aritmético");
		double resultado = 0;

		//Switch para comprobar simbolo
		switch (aritmetico) {
		case "+":
			resultado = operador1 + operador2;
			JOptionPane.showMessageDialog(null, "Resultado: "+resultado);

			break;
		case "-":
			resultado = operador1 - operador2;
			JOptionPane.showMessageDialog(null, "Resultado: "+resultado);
			
			break;
		case "*":
			resultado = operador1 * operador2;
			JOptionPane.showMessageDialog(null, "Resultado: "+resultado);
			
			break;
		case "/":
			resultado = operador1 / operador2;
			JOptionPane.showMessageDialog(null, "Resultado: "+resultado);
			
			break;			
		case "^":
			resultado = Math.pow(operador1, operador2);
			JOptionPane.showMessageDialog(null, "Resultado: "+resultado);
			
			break;			
		case "%":
			resultado = operador1 % operador2;
			JOptionPane.showMessageDialog(null, "Resultado: "+resultado);
			
			break;
		default:
			break;
		}


	}

}
