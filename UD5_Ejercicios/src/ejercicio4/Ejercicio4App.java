package ejercicio4;
import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio4App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Pedimos el radio
		String entrada = JOptionPane.showInputDialog("Introduce el radio");
		
		//Pasamos de String a Double el numero introducido por el usuario
		double radio = Double.parseDouble(entrada);
		double resultado = Math.PI * Math.pow(radio, 2);

		//Mostramos el resultado
		JOptionPane.showMessageDialog(null, "El area del circulo es:  "+resultado);


	}

}
