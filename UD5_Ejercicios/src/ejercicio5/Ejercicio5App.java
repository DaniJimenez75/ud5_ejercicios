package ejercicio5;
import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String entrada = JOptionPane.showInputDialog("Introduce un numero");
		
		//Pasamos numero a Double
		double numero = Double.parseDouble(entrada);
		
		//Si el residuo es 0 es divisible por 2
		if(numero % 2 == 0) {
		JOptionPane.showMessageDialog(null, "Es divisible por 2");

		}else {
		JOptionPane.showMessageDialog(null, "No es divisible por 2");
			
		}
		


	}

}
