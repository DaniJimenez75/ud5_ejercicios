/**
 * 
 */
package ejercicio12;

import javax.swing.JOptionPane;

/**
 * @author Dani
 *
 */
public class Ejercicio12 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int intentos = 0;
		boolean acertado = false;
		String contraseņa = "java";
		do {
			String entrada = JOptionPane.showInputDialog("Introduce contraseņa");			
			
			//Si la contraseņa es igual muestra el mensaje y acertado pasa a true
			if(contraseņa.equals(entrada)) {
				JOptionPane.showMessageDialog(null, "Enhorabuena");
				acertado=true;
			}else {
				intentos++; //incrementamos 1 intento
			}
			
			
		}while(intentos!=3 && !acertado); //No sale hasta que los intentos sea igual a 3 o acertado sea true


	}

}
