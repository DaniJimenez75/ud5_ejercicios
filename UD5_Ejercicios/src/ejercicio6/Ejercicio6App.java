package ejercicio6;
import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio6App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String entrada = JOptionPane.showInputDialog("Introduce precio del producto");
		
		//Pasamos numero a Double
		double numero = Double.parseDouble(entrada);
		
		double IVA = 0.21;
		double precioFinal = numero + (numero*IVA); //Le sumamos el IVA al precio inicial
		JOptionPane.showMessageDialog(null, "El precio con IVA es:  "+precioFinal);

		
		

	}

}
